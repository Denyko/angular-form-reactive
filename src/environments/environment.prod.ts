import { Environment } from './environment.module';

export const environment = {
  production: true,
  apiRoot: 'https://fainna.herokuapp.com/api/'
};
