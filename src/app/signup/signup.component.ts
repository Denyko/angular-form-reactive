import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AuthService } from '../providers/auth.service';
import { ProfileService } from '../providers/profile.service';
import { Observable } from 'rxjs/Observable';
import "rxjs/add/operator/switchMap";
import "rxjs/add/observable/forkJoin";



@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

public loginForm: FormGroup;

  constructor(private auth: AuthService, private profile: ProfileService) {}
  
  ngOnInit() {
    this.loginForm = new FormGroup({
    	credentials: new FormGroup({
    		email: new FormControl('', [
	        Validators.required,
	        Validators.email
	        ]),
	      	password: new FormControl('', [
	        Validators.required, 
	        Validators.minLength(8)
	        ])
    	}),
    	profile: new FormGroup({
    		phone: new FormControl('', [
	        Validators.required, 
	        Validators.minLength(13),
	        Validators.maxLength(14),
	        Validators.pattern("\\+[0-9 ]+")
	        ]),
    		firstName: new FormControl('', [
		      	Validators.required,
		        Validators.pattern("[a-zA-Z][a-zA-Z ]+")
	        ]),
		    lastName: new FormControl('', [
		        Validators.pattern("[a-zA-Z][a-zA-Z ]+")
	        ]),
	      	gender: new FormControl('', [
		      	Validators.required
	      	]),
	      	dateOfBirth: new FormControl('', [
	      		Validators.required,
	      		// Validators.pattern("^\d{1,2}\/\d{1,2}\/\d{4}$")
	      	]),
	      	githubUrl: new FormControl('', [
	      		Validators.required
	      		// Validators.pattern("")
	      	])
    	}),
    	address: new FormGroup({
    		country: new FormControl('' , []),
    		city: new FormControl('' , []),
    		street: new FormControl('' , []),
    		zip: new FormControl('' , [])
    	}) 
    });
  }

  isControlInvalid(formGroupName: string, controlName: string) {
    const control = this.loginForm.get(formGroupName).get(controlName);
    return control.dirty && control.invalid;
  }


  onSubmit() {
    
    const item = {
      "country": "China",
      "city": "Bahaodi",
      "street": "Melody",
      "zip": "508"
    };
    const item1 = {
      "country": "Armenia",
      "city": "Griboyedov",
      "street": "Blackbird",
      "zip": "4"
    };
    const item2 = {
      "country": "Russia",
      "city": "Sokolovyy",
      "street": "Hooker",
      "zip": "3060"
    };

    
    this.auth
      .signup(this.loginForm.value.credentials)
      .switchMap(res => this.profile.create(this.loginForm.value.profile))
      // .switchMap(res => this.profile.addAddress(this.loginForm.value.address, res.id))
      .switchMap(response => Observable.forkJoin(
        this.profile.addAddress(item, response.id),
        this.profile.addAddress(item1, response.id),
        this.profile.addAddress(item2, response.id)
      ))
      .subscribe(res => {
        console.log(res);
      });
  }
}
