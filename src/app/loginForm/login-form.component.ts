import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AuthService } from '../providers/auth.service';
import { ProfileService } from '../providers/profile.service';
@Component({
  moduleId: module.id,
  selector: 'login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {
  
  public loginForm: FormGroup;

  constructor(private auth: AuthService, private profile: ProfileService ) {}
  
  ngOnInit() {
    this.loginForm = new FormGroup({
      email: new FormControl('', [
        Validators.required,
        Validators.email
        ]),
      password: new FormControl('', [
        Validators.required, 
        Validators.minLength(8)
        ])
    });
  }

  isControlInvalid(controlName: string) {
    const control = this.loginForm.get(controlName);
    return control.dirty && control.invalid;
  }
  isControlValid(controlName: string) {
    const control = this.loginForm.get(controlName);
    return control.dirty && control.valid;
  }

  onSubmit()  {
    this.auth
      // .login(this.loginForm.value).subscribe(res => {
      //   console.log(res);
      // });
      .login(this.loginForm.value)
      .do(res => console.log(res))
      .switchMap(res =>  this.profile.get())
      .subscribe(res => console.log(res));
    }
  


}