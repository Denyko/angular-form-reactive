import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http'
import { AppComponent } from './app.component';
import { LoginFormComponent } from './loginForm/login-form.component';
import { SignupComponent } from './signup/signup.component';
import { EnvService } from './providers/env.service';
import { AuthService } from './providers/auth.service';
import { IdentityService} from './providers/identity.service';
import { ProfileService } from './providers/profile.service';



@NgModule({
  declarations: [
    AppComponent,
    LoginFormComponent,
    SignupComponent
  ],

  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule
  ],

  providers: [
    EnvService,
    AuthService,
    IdentityService,
    ProfileService
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
