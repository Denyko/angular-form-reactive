import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable()
export class EnvService {

public readonly apiUrl: string;

  constructor() { 
  	this.apiUrl = environment.apiRoot;
  }
}
