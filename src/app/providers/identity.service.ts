import { Injectable } from '@angular/core';  

@Injectable()
export class IdentityService {

  private readonly key: string;
  private readonly token: string;

  constructor() {
    this.key = 'user-id';
    this.token = 'user-token';
  }

  public getUserKey () {
    return localStorage.getItem(this.key);
  }

  public setUserKey(value: string) {
    localStorage.setItem(this.key, value);
  }


  public getTokenKey() {
    return localStorage.getItem(this.token);
  }

  public setTokenKey(value: string) {
    localStorage.setItem(this.token, value);
  }

  public remove() {
    localStorage.removeItem(this.key);
  }
}
