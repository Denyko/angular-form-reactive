import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { IdentityService } from './identity.service';
import { EnvService } from './env.service';


export interface ProfileResponse {
  phone: number;
  firstName: string;
  lastName: string;
  gender: string;
  dateOfBirth: string;
  githubUrl: string;
  id: number;
  userId: string;
}

export interface AddressResponse {
  country: string;
  city: string;
  street: string;
  zip: number;
  id: number;
  profileId: number;
}

export interface UserResponse {
  realm: string;
  username: string;
  email: string;
  emailVerified: boolean;
  id: number;
  }

@Injectable()

export class ProfileService {

  constructor(
  	private http: HttpClient, 
  	private identity: IdentityService,
  	private env: EnvService
  	) { }

  public create(profile) {
    const result = Object.assign({}, profile, { userId: this.identity.getUserKey()});
    return this.http.post<ProfileResponse>(`${this.env.apiUrl}/profiles`, result);
  }

  public addAddress(address, profileId) {
    const body = Object.assign({}, address, { profileId: profileId});
    return this.http.post<AddressResponse>(`${this.env.apiUrl}/profiles/${profileId}/addresses`, body);
  } 

  public get() {
    // debugger
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    headers.append( 'Authorization', this.identity.getTokenKey());
    return this.http.get<UserResponse>(`${this.env.apiUrl}/fainna-users/${this.identity.getUserKey()}`, {headers:headers})
  }
}
