import { Injectable, EventEmitter} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EnvService } from './env.service';
import { IdentityService } from './identity.service';
import 'rxjs/add/operator/do';

export interface LoginResponse {
  id: string;
  ttl: number;
  created: Date & string;
  userId: string;
}


export interface RegisterResponse {
  username?: string;
  email: string;
  id: string;
}

@Injectable()

export class AuthService {
  public onAuthStateChange: EventEmitter<boolean>;
	
  constructor(
  	private http: HttpClient, 
  	private env: EnvService, 
  	private identity: IdentityService
  	) {
  	this.onAuthStateChange = new EventEmitter();
  }

  public login(credentials) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    headers.append( 'Authorization', this.identity.getTokenKey());

  	return this.http.post<LoginResponse>(`${this.env.apiUrl}/fainna-users/login`, credentials, {headers: headers})
  		.do(response => {
        this.identity.setUserKey(response.userId);
  			this.identity.setTokenKey(response.id);
  			this.onAuthStateChange.emit(true);
  		});
  }

  public logout() {
  	return this.http.post(`${this.env.apiUrl}/fainna-users/logout`, null)
  		.do(response => {
  			this.identity.setUserKey(null);
  			this.identity.setTokenKey(null);
  			this.onAuthStateChange.emit(false);
  		});
  }

  public signup(credentials) {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    headers.append( 'Authorization', this.identity.getTokenKey());
    
  	return this.http.post<RegisterResponse>(`${this.env.apiUrl}/fainna-users`, credentials, {headers:headers})
      .do(res => {
        this.identity.setUserKey(res.id);
      });
  }
}
